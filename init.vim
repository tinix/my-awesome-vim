" vim:et sw=2 ts=2 fdm=marker
" Author: Daniel Tinivella <dtinivella@gmail.com>
" GitHub: https://github.com/tinix

" ------------------------------ Plugins ( VimPlug ) ------------------------------
call plug#begin('~/.vim/plugged')

Plug 'sainnhe/sonokai' " Color scheme.
Plug 'sheerun/vim-polyglot' " A collection of language packs for Vim.
Plug 'ap/vim-css-color' " Context-sensitive color name highlighter.
Plug 'junegunn/goyo.vim' " Distraction-free writing in Vim.

Plug 'vim-airline/vim-airline' " Status/tabline for vim that's light as air.
Plug 'tpope/vim-fugitive' " Fugitive is the premier Vim plugin for Git.
Plug 'morhetz/gruvbox' " Theme Gruvbox

Plug 'junegunn/fzf' " Fzf is a general-purpose command-line fuzzy finder.
Plug 'junegunn/fzf.vim' " Vim plugin for basic wrapper funtion to FZF.
Plug 'airblade/vim-rooter'

Plug 'SirVer/ultisnips' " The ultimate solution for snippets in Vim.
Plug 'dense-analysis/ale' " ALE (Asynchronous Lint Engine).
Plug 'prettier/vim-prettier' " A vim plugin wrapper for prettier.
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' } " Completion framework for neovim/Vim8.
Plug 'tbodt/deoplete-tabnine', { 'do': './install.sh' } " Deep learning to help you write code faster.
Plug 'skywind3000/asyncrun.vim' " Run Async Shell Commands
Plug 'easymotion/vim-easymotion'
" Emmet
Plug 'mattn/emmet-vim'
" Float Terminal in nvim
Plug 'voldikss/vim-floaterm'
" Multiple cursores
Plug 'terryma/vim-multiple-cursors'
" Vertical Line | plugin to indent"
Plug 'Yggdroot/indentLine'
" nvim-telescope/telescope.nvim "
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

Plug 'neoclide/coc.nvim', { 'branch': 'release' }
Plug 'amiralies/coc-elixir', {'do': 'yarn install && yarn prepack'}
Plug 'jpo/vim-railscasts-theme'

call plug#end()

" ---------------------------- Plugin Settings ------------------------------
source ~/.config/nvim/plugin_settings.vim
" ---------------------------- General Settings -----------------------------
source ~/.config/nvim/general_settings.vim
" ----------------------------- Key Bindings --------------------------------
source ~/.config/nvim/key_bindings.vim
