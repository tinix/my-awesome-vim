" ---------------------------- General Settings -----------------------------

set number
set mouse=a
syntax enable
syntax on
set autoindent
set cursorline
set expandtab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set termguicolors
" set numberwidth=1
set clipboard=unnamed
set showcmd
set ruler
set encoding=utf-8
set showmatch
set sw=2
set laststatus=2
" set noshowmode
set smarttab
"set relativenumber
"ctags
set tags=./tags;/
"" Encoding
set fileencoding=utf-8
"" Tabs. May be overridden by autocmd rules
set modeline
set modelines=10
set title
set titleold="Terminal"
set titlestring=%F
set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

"command S e ~/.config/nvim/init.vim
nnoremap ,<space> :nohlsearch<CR>
nnoremap <Space> za
" Run jest test for current file
command Test !npx jest %
